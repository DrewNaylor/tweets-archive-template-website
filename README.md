# tweets-archive-template-website
A template that can be used to host archived Tweets based off my own website's version. Please note that this template isn't up and running on Codeberg Pages yet, so it can't properly be marked as a template repo.

[View the template's example website](https://drew-naylor.com/tweets-archive-template-website/)

I also host an example of what a tweet archive website could look like with actual, non-example content on my website: https://drew-naylor.com/pages/old-project-sites/bestest-tweets-archive/
